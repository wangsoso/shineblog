drop table if exists tm_category;
drop table if exists tm_link;
drop table if exists tm_blog;
drop table if exists tm_entry;
drop table if exists tm_user;
drop table if exists tm_comment;
create table tm_category (
   id integer not null auto_increment,
   blog_id integer not null,
   display_order integer not null,
   name varchar(20) not null,
   primary key (id)
);
create table tm_link (
   id integer not null auto_increment,
   blog_id integer not null,
   display_order integer not null,
   name varchar(20) not null,
   url varchar(50) not null,
   primary key (id)
);
create table tm_blog (
   id integer not null auto_increment,
   about text not null,
   comments_need_be_approved integer not null,
   description varchar(100) not null,
   entry_range integer not null,
   recent_comment_range integer not null,
   recent_entry_range integer not null,
   status integer not null,
   title varchar(20) not null,
   primary key (id)
);
create table tm_entry (
   id integer not null auto_increment,
   allow_comments integer not null,
   category_id integer not null,
   comment_count integer not null,
   content text not null,
   create_on datetime,
   name varchar(100) not null,
   status integer not null,
   primary key (id)
);
create table tm_user (
   id integer not null auto_increment,
   name varchar(20) not null,
   password varchar(50) not null,
   primary key (id)
);
create table tm_comment (
   id integer not null auto_increment,
   author varchar(10) not null,
   content varchar(200) not null,
   create_on datetime not null,
   email varchar(50) not null,
   entry_id integer not null,
   ip varchar(15) not null,
   status integer not null,
   url varchar(50) not null,
   primary key (id)
);
alter table tm_category add index FK834E1804FEBA86D8 (blog_id), add constraint FK834E1804FEBA86D8 foreign key (blog_id) references tm_blog (id);
alter table tm_link add index FKB7C07E00FEBA86D8 (blog_id), add constraint FKB7C07E00FEBA86D8 foreign key (blog_id) references tm_blog (id);
alter table tm_entry add index FK3FEEFB2C5BA8ABFC (category_id), add constraint FK3FEEFB2C5BA8ABFC foreign key (category_id) references tm_category (id);
alter table tm_comment add index FK97A01A9983353C08 (entry_id), add constraint FK97A01A9983353C08 foreign key (entry_id) references tm_entry (id);
